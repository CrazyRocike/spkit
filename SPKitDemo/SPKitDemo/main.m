//
//  main.m
//  SPKitDemo
//
//  Created by Ho Aici on 2018/12/10.
//  Copyright © 2018 Ho Aici. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
