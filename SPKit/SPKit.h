//
//  SPKit.h
//  SPKit
//
//  Created by Ho Aici on 2018/12/10.
//  Copyright © 2018 Ho Aici. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SPKit.
FOUNDATION_EXPORT double SPKitVersionNumber;

//! Project version string for SPKit.
FOUNDATION_EXPORT const unsigned char SPKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SPKit/PublicHeader.h>

#import "SPSegmentPage.h"

