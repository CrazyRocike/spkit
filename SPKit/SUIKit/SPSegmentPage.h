//
//  SPSegmentPage.h
//  SPKit
//
//  Created by Ho Aici on 2018/12/10.
//  Copyright © 2018 Ho Aici. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN



@interface SPSegmentPageContainer : UIView

@end

@interface SPSegmentPage : UIView<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *segmentPage;
@property (nonatomic, strong) UICollectionViewLayout *segmentPageLayout;
@property (nonatomic, strong) UIColor *sliderColor;
@property (nonatomic, strong) UIColor *itemFount;
@property (nonatomic, strong) UIColor *itemNormalColor;
@property (nonatomic, strong) UIColor *itemSelectedColor;
@property (nonatomic, strong) SPSegmentPageContainer *segmentPageContainer;
@property (nonatomic, strong) NSArray *segmentPageItems;
@property (nonatomic, assign) NSInteger selectedIndex;
@end

NS_ASSUME_NONNULL_END
