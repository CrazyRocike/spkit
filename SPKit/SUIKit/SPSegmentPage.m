//
//  SPSegmentPage.m
//  SPKit
//
//  Created by Ho Aici on 2018/12/10.
//  Copyright © 2018 Ho Aici. All rights reserved.
//

#import "SPSegmentPage.h"

@implementation SPSegmentPage

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.segmentPageLayout) {
            self.segmentPage = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.segmentPageLayout];
        }else{
            self.segmentPage = [[UICollectionView alloc] init];
        }
        // add view
        [self addSubview:self.segmentPage];
        // config delegate
        self.segmentPage.delegate = self;
        self.segmentPage.dataSource = self;
        // config layout
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.segmentPage
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self
                                                               attribute:NSLayoutAttributeTop
                                                              multiplier:1.0
                                                                constant:0];
        NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.segmentPage
                                                               attribute:NSLayoutAttributeLeft
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self
                                                               attribute:NSLayoutAttributeLeft
                                                              multiplier:1.0
                                                                constant:0];
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.segmentPage
                                                               attribute:NSLayoutAttributeBottom
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self
                                                               attribute:NSLayoutAttributeBottom
                                                              multiplier:1.0
                                                                constant:0];
        NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:self.segmentPage
                                                               attribute:NSLayoutAttributeRight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self
                                                               attribute:NSLayoutAttributeRight
                                                              multiplier:1.0
                                                                constant:0];
        [self addConstraints:@[top, left, right, bottom]];
        
       
        
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- 


@end
